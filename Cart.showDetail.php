<?php
session_start();
require("orderModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Examples</title>
</head>
<body>
<p>This is the Shopping Cart Detail 
[<a href="logout.php">logout</a>]
</p>
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<HR>";
	$result=getCartDetail($_SESSION["loginProfile"]["uID"]);
?>
<hr>
<table width="400" border="1" height="200">
  <tr>
    <!--<td>id</td>-->
    <td>Prd Name</td>
    <td>price</td>
    <td>Quantity</td>
    <td>Amount</td>
	
  </tr>
<?php
$total=0;
$num = array();
$tmp = array();
$last = array();
$count = array();
$se = array(array(),array());
$cost = array();
$resultpr=getPrdList();
$n = 1;
$i = 0;
$t = 0;

$tmp[0]=null;
$tmp[2]=0;
$last[0] = $tmp[0];
while ($r=mysqli_fetch_assoc($result))
{
    if($tmp[0] == null){
        $tmp[0] = $r['name'];
        $tmp[1] = $r['price'];
        //$tmp[2] = $r['quantity'];
        $tmp[3] = $r['serno'];
    }
    if($tmp[0] != $r['name']){
    echo "<td>",$tmp[0],"</td>";
    echo "<td>" , $tmp[1], "</td>";
    echo "<td>" , $tmp[2], "</td>";
    $total += $tmp[2]*$tmp[1];
    echo "<td>" , $tmp[2]*$tmp[1] , "</td>";
    echo "<td><a href='Cart.removeItem.php?serno=" , $tmp[3] , "'>Remove</a></td></tr>";
        $tmp[0] = $r['name'];
        $tmp[1] = $r['price'];
        $tmp[2] = 0;
        $tmp[3] = $r['serno'];
        $last[0] = $tmp[0];
        $last[1] = $tmp[1];
        $last[2] = $tmp[2];
        $last[3] = $tmp[3];
    }
        $tmp[3] = $r['serno'];
        $last[3] = $tmp[3];
        $tmp[2]++;
}
if($last[0] != $tmp[0] || $tmp[2] != 0)
{echo "<td>",$tmp[0],"</td>";
    echo "<td>" , $tmp[1], "</td>";
    echo "<td>" , $tmp[2], "</td>";
    $total += $tmp[2] *$tmp[1];
    echo "<td>" , $tmp[2]*$tmp[1] , "</td>";
    echo "<td><a href='Cart.removeItem.php?serno=" , $tmp[3] , "'>Remove</a></td></tr>";
}
echo "<tr><td>Total: $total</td></tr>";
?> 
</table>
<hr>
<a href="order.confirm.php">Yes Checkout</a>  <a href="main.php">No, keep shopping</a>
</body>
</html>
